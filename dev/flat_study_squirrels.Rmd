---
title: "flat_minimal.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
library(glue)
library(assertthat)
library(ggplot2)
library(dplyr)
```

<!--
 You need to run the 'description' chunk in the '0-dev_history.Rmd' file before continuing your code there.
-->

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```

# Get a message with a fur color: get_message_fur_color

You can get a message with the fur color of interest with `get_message_fur_color()`.
    
```{r function-get_message_fur_color}
#' Get a message with the fur color of interest.
#'
#' @param primary_fur_color Character. The primary fur color of interest
#' 
#' @importFrom glue glue
#'
#' @return Side effect. Message with color interest
#' @export
#'
#' @examples
get_message_fur_color <- function(primary_fur_color) {
  message(glue("We will focus about {primary_fur_color} squirrels"))
}
```
  
```{r example-get_message_fur_color}
get_message_fur_color(primary_fur_color = "Red")
get_message_fur_color(primary_fur_color = "Black")
```
  
```{r tests-get_message_fur_color}
test_that("get_message_fur_color works", {

  expect_message(
    object = get_message_fur_color(primary_fur_color = "Cinnamon"),
    regexp = "We will focus about Cinnamon squirrels"
  )
  expect_message(
    object = get_message_fur_color(primary_fur_color = "Black"),
    regexp = "We will focus about Black squirrels"
  )
})

test_that("get_message_fur_color doesnt work", {

  expect_error(
    expect_message(
      object = get_message_fur_color(primary_fur_color = "Black"),
      regexp = "We will focus on Cinnamon squirrels"
    )
  )
 
})
```
  
# study_activity : expliquer plus ma fonction
    
```{r function-study_activity}
#' study_activity
#'
#' @param df_squirrels_act data.frame. squirrels
#' @param col_primary_fur_color character. color
#' 
#' @importFrom assertthat assert_that
#' @importFrom ggplot2 ggplot aes geom_col labs scale_fill_manual
#' @importFrom dplyr filter
#' @importFrom glue glue
#'
#' @return a list
#' @export
#'
#' @examples
study_activity <- function(df_squirrels_act, col_primary_fur_color) {
  
  assert_that(is.data.frame(df_squirrels_act))
  assert_that(is.character(col_primary_fur_color))
  
  check_squirrel_data_integrity(df_squirrels = df_squirrels_act)
  
  table <- df_squirrels_act %>% 
    filter(primary_fur_color == col_primary_fur_color)
    
  graph <- table %>% 
    ggplot() +
    aes(x = activity, y = counts, fill = age) +
    geom_col() +
    labs(x = "Type of activity",
         y = "Number of observations",
         title = glue("Type of activity by age for {tolower(col_primary_fur_color)} squirrels")) +
    scale_fill_manual(name = "Age",
                      values = c("#00688B", "#00BFFF"))
  
  return(list(table = table, graph = graph))
}
```
  
```{r example-study_activity}
data(data_act_squirrels)
study_activity(df_squirrels_act = data_act_squirrels, 
               col_primary_fur_color = "Gray")
```
  
```{r tests-study_activity}
test_that("study_activity works", {
  expect_true(inherits(study_activity, "function"))
  
  data(data_act_squirrels)
  res_activity <- study_activity(
    df_squirrels_act = data_act_squirrels, 
    col_primary_fur_color = "Gray"
  )
  
  expect_equal(
    object = names(res_activity),
    expected = c("table", "graph")
  )
  
  expect_true(inherits(res_activity[["table"]], c("spec_tbl_df", "tbl_df", "tbl", "data.frame")))
  
  expect_true(inherits(res_activity[["graph"]], c("gg", "ggplot")))
})
```

# Une nouvelle fonction pour sauvegarder du csv: `save_as_csv`
    
```{r function-save_as_csv}
#' save_as_csv
#'
#' @param data data.frame. tableau qu on veut enregistrer et stocker
#' @param path character. chemin vers les donnees
#' 
#' @importFrom assertthat assert_that is.writeable has_extension is.dir
#' @importFrom utils write.csv2
#'
#' @return the path 
#' @export
#'
#' @examples
save_as_csv <- function(data, path) {
  
  assert_that(is.data.frame(data))
  assert_that(is.character(path))
  assert_that(is.dir(dirname(path)))
  assert_that(has_extension(path, ext = "csv"))
  assert_that(is.writeable(dirname(path)))
  
  write.csv2(x = data, file = path)
  
  return(invisible(path))
}
```
  
```{r example-save_as_csv}
temp_file <- tempfile(pattern = "savecsv")
dir.create(temp_file)

# ok : le fichier .csv est enregistré dans le projet actuel
res <- iris %>% 
  save_as_csv(path = file.path(temp_file, "output.csv"))
res
```
  
```{r tests-save_as_csv}
test_that("save_as_csv works", {
  expect_true(inherits(save_as_csv, "function"))
  
  temp_file <- tempfile(pattern = "savecsv")
  dir.create(temp_file)
  
  res <- iris %>% 
    save_as_csv(path = file.path(temp_file, "output.csv"))
  
  expect_true(file.exists(res))

})

test_that("save_as_csv doesnt works", {
  
  temp_file <- tempfile(pattern = "savecsv")
  dir.create(temp_file)
  
  expect_error(
    iris %>% 
      save_as_csv(path = file.path(temp_file, "output.R")),
    regexp = "File 'output.R' does not have extension csv"
  )
  
  expect_error(
    iris %>% save_as_csv("/usr/bin/output.csv"),
    regexp = "Path '/usr/bin' is not writeable"
  )
})
```
  


```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(
  flat_file = "dev/flat_study_squirrels.Rmd", 
  vignette_name = "Study the squirrels"
)
```
