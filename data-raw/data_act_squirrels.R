library(tidyverse)

data_act_squirrels <- read_csv(file = "data-raw/nyc_squirrels_act_sample.csv")

data_act_squirrels <- data_act_squirrels %>%
  slice_head(n = 15)

usethis::use_data(data_act_squirrels, overwrite = TRUE)

checkhelper::use_data_doc(name = "data_act_squirrels")
attachment::att_amend_desc()
