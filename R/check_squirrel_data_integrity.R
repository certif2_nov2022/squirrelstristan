# WARNING - Generated by {fusen} from /dev/flat_check_data.Rmd: do not edit by hand

#' check squirrel data interity
#'
#' @param df_squirrels data.frame with primary_fur_color
#'
#' @return side effect. message
#' @export
#'
#' @examples
#' 
#' nyc_squirrels_sample <- readr::read_csv(
#'   system.file("nyc_squirrels_sample.csv", package = "squirrelstristan")
#' )
#' 
#' # nyc_squirrels_sample <- nyc_squirrels_sample %>% 
#' # dplyr::rename(color = primary_fur_color)
#' 
#' check_squirrel_data_integrity(df_squirrels = nyc_squirrels_sample)
check_squirrel_data_integrity <- function(df_squirrels){
  
  check_name_exist <- "primary_fur_color" %in% names(df_squirrels)
  
  if (isFALSE(check_name_exist)) {
    stop("There is no column primary_fur_color")
  }
  
  primary_is_ok <- check_primary_color_is_ok(df_squirrels$primary_fur_color)
  
  if (isTRUE(primary_is_ok)) {
    message("All primary fur color are ok ")
  }
}
